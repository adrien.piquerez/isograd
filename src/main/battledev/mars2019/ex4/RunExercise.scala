package battledev.mars2019.ex4

class RunExercise {
  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val words: Seq[String] = input.drop(1)

    val possibilities = words.head.foldLeft(Seq(Seq[Char]())){
      case(seq, c) => seq.map(s => s :+ c) ++ seq
    }.toList.sortBy(s => -s.size)

    val res = possibilities.filter { seq =>
      words.tail.forall{ word  =>
        word.foldLeft(seq) {
          (s, c) => if (s.headOption.contains(c)) s.tail else s
        }.isEmpty
      }
    }

    val result = res.head.mkString("")
    out { if(result.isEmpty) "KO" else result }
  }

  def out(line: String) = println(line)
}

