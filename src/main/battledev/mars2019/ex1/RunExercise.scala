package battledev.mars2019.ex1

class RunExercise {
  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val n = input.head.toInt
    val res = input.drop(1).map(s => s.split(" ")).map(a => - a(1).toInt + a(0).toInt).sum + n
    out { if(res <= 100) 1000 else if (res <= 10000) 100 else "KO" }
  }

  def out(line: Any) = println(line)
}
