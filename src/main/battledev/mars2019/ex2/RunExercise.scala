package battledev.mars2019.ex2

class RunExercise {
  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    out {
      input.drop(1).map(s => s.toInt).foldLeft((0, 0)) {
        case ((res, state), next) =>
          if (state + next > 100) (res + 1, next)
          else (res, state + next)
      }._1 + 1
    }
  }

  def out(line: Any) = println(line)
}
