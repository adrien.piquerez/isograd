package battledev.mars2019.ex3

import scala.collection.SortedSet

class RunExercise {
  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val map = input.drop(1).zipWithIndex.flatMap { case (s, i) => s.zipWithIndex.map { case (c, j) => (i, j) -> c  }}
    val coins = SortedSet(map.filter(_._2 == 'o').map(_._1): _*)
    val mults = SortedSet(map.filter(_._2 == '*').map(_._1): _*)

    def travel(x: Int, y: Int, set: SortedSet[(Int, Int)]): (Seq[Char], (Int, Int)) = {
      if(set.isEmpty) (Seq(), (x, y))
      else {
        val (hx, hy) = set.head
        val tail = set.tail

        val toX = if (x <= hx) (x until hx).map(_ => 'v') else (hx until x).map(_ => '^')
        val toY =  if (y <= hy) (y until hy).map(_ => '>') else (hy until y).map(_ => '<')

        val p = travel(hx, hy, tail)
        ((toX ++ toY :+ 'x') ++ p._1, p._2)
      }
    }

    out {
      travel(0, 0, coins) match  {
        case (pc, pos) => (pc ++ travel(pos._1, pos._2, mults)._1).mkString("")
      }
    }
  }

  def out(line: String) = println(line)
}
