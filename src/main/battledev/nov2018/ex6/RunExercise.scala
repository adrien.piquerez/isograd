package battledev.nov2018.ex6

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val n = input.head.toInt
    val mid = n.toFloat / 2F
    val f = input(1).split(" ").map(s => s.toInt)
    val iter = input(2).toInt

    val u0 = f.dropRight(1).zip(f.drop(1)).map { case (a, b) => if ((a < mid && b  > mid) || (a > mid && b < mid)) 1 else 0 }
    val recur = f.dropRight(1).zip(f.drop(1)).map { case (a, b) => if(a < b) a until b else if (a > b) b until a else Seq() }

    out((0 until iter - 1).foldLeft(u0) { case (u, _) => recur.map( seq => seq.map(id => u(id)).sum % 1000) }.sum % 1000)
  }

  def out(line: Any) = println(line)
}
