package battledev.nov2018.ex3

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    val mid = input.head.toFloat / 2F

    val row = input(1).split(' ').map(_.toFloat)
    val pairs = row.drop(1).zip(row.dropRight(1))

    if (pairs.exists { case (a, b) => a == mid  && b == mid })
      inf
    else
      out { pairs.count { case (a, b) => (a < mid && b > mid) || (a > mid && b < mid) } + row.count(_ == mid) }
  }

  def out(line: Any) = println(line)
  def inf = println("INF")
}
