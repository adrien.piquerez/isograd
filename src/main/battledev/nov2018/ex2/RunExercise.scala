package battledev.nov2018.ex2

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    out {
      input.drop(1).toSet.count { w =>
        w.length >= 5 && w.length <= 7 &&
          w(0) + 1 == w(1) &&
          Set('a', 'e', 'i', 'o', 'u', 'y').contains(w.last)
      }
    }
  }

  def out(line: Any) = println(line)
}
