package battledev.nov2018.ex5

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]): Unit = {
    val initialState = input.drop(1).map(s => s.split(' ').map(_.toInt)).zipWithIndex.map {
      case (a, i) =>  i -> Seq(Proposal(i, a(0), 1), Proposal(i, a(1), 2))
    }.toMap

    val timeLine = initialState.values.flatten.toList.sortBy(p => p.time)
    timeLine.dropRight(1).zip(timeLine.tail).foreach{ case (a, b) => a.next = Some(b); b.prev = Some(a) }

    solve(initialState).map(out).getOrElse(ko)
  }

  case class State(result: Seq[Proposal], rest: Map[Int, Seq[Proposal]])

  def solve(initialState: Map[Int, Seq[Proposal]]): Option[Seq[Int]] = {
    initialState.foldLeft(Option(State(Seq(), initialState))){
      case (Some(state), (student, _)) =>
        if(!state.rest.contains(student)) Some(state)
        else if (state.rest(student).isEmpty) None
        else state.rest(student).toStream.flatMap(p => take(p, state)).headOption
      case _ => None
    }.map { finalState =>
      finalState.result.sortBy(r => r.student).map(r => r.order)
    }
  }

  def take(taken: Proposal, state: State): Option[State] = {
    if(!state.rest.contains(taken.student)) Some(state)
    else {
      val toRemove = (unfold(taken)(h => h.prev).takeWhile(p => p.time >= taken.time - 60) ++
                      unfold(taken)(h => h.next).takeWhile(p => p.time <= taken.time + 60))
        .filter(p => state.rest.contains(p.student))

      val rest = toRemove.foldLeft(state.rest){
        case (s, p) => s.updated(p.student, s(p.student).filter(o => o != p))
      } - taken.student

      toRemove.foldLeft(Option(State(taken +: state.result, rest))){
        case (Some(s), p) if s.rest.contains(p.student) => s.rest(p.student).headOption.flatMap(p => take(p, s))
        case (opt, _) => opt
      }
    }
  }

  private def unfold[A](start: A)(f: A => Option[A]): Stream[A] = f(start) match {
    case Some(next) => next #:: unfold(next)(f)
    case None => Stream.empty
  }

  case class Proposal(student: Int, time: Int, order: Int) {
    var next: Option[Proposal] = None
    var prev: Option[Proposal] = None
  }

  def out(lines: Traversable[Any]) = lines.foreach(println)
  def ko = println("KO")
}

