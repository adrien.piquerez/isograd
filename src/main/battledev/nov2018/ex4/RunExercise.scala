package battledev.nov2018.ex4

import scala.collection.mutable

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    val all = input.drop(1).map(s => s.split(' ').map(_.toInt)).map(a => (a(0), a(1)))
    out(max(all, Seq()))
  }

  def max(all: Seq[(Int, Int)], taken: Seq[Int]): Int = {
    if(all.isEmpty) 0 else {
      val (t1, t2) = all.head
      val tail = all.tail

      val candidates = mutable.Buffer(max(tail, taken))
      if(ok(t1, taken)) candidates += max(tail, taken :+ t1) + 1
      if(ok(t2, taken)) candidates += max(tail, taken :+ t2) + 1

      candidates.max
    }
  }

  def ok(t: Int, taken: Seq[Int]): Boolean = {
    taken.forall(o => math.abs(t - o) > 60)
  }

  def out(line: Any) = println(line)
}
