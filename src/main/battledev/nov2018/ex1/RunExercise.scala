package battledev.nov2018.ex1

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    val min = input(1).toInt
    val (max, name) = input.drop(2).map(s => s.split(" ")).map(a => a(0).toInt -> a(1)).maxBy(_._1)

    if(max > min) out(name) else ko
  }

  def out(line: String) = println(line)
  def ko = println("KO")
}
