package battledev.mars2018.ex6

import scala.collection.mutable

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  val cat = new Node
  val patient = new Node

  def solve(input: Seq[String]) = {
    val n = input.head.toInt
    val map = input.drop(1).map(s => s.collect {
      case '.' => new Room
      case '?' => new Door
      case 'c' => new Cat
      case 'p' => new Patient
      case '#' => new Wall
    })

    for {
      i <- 0 until n
      j <- 0 until n
      (k, l) <- Seq((i - 1, j), (i, j-1), (i+ 1, j), (i, j+1))
      if k >= 0 && l >= 0 && k < n && l < n
    } map(i)(j).connectTo(map(k)(l))

    def path(from: Node, dejaVue: mutable.Set[Node]): Option[Seq[Step]] = {
      if (from == patient) Some(Seq())
      else (from.to.map(new AStep(_)).toStream ++ from.from.map(new RStep(_)).toStream)
        .filter(s => !dejaVue.contains(s.to) && s.capacity > 0)
        .flatMap(e => path(e.to, dejaVue += e.to).map(p => e +: p)).headOption
    }

    val allPath = Iterator.continually(path(cat, mutable.Set(cat)))

    out {
      val result = allPath.takeWhile(_.isDefined).flatten.map{ p =>
        val min = p.map(e => e.capacity).min
        p.foreach(e => e.use(min))
        min
      }.sum

      if (result < -1000 || result > 1000) - 1 else result
    }
  }

  trait Step {
    def capacity: Int
    def to: Node
    def use(u: Int): Unit
  }

  class AStep(e: Edge) extends Step {
    override def capacity: Int = e.capacity - e.used
    override def to: Node = e.to
    override def use(u: Int): Unit = e.used = e.used + u
  }
  class RStep(e: Edge) extends Step {
    override def capacity: Int = e.used
    override def to: Node = e.from
    override def use(u: Int): Unit = e.used = e.used - u
  }

  class Room extends Node with Square

  class Wall extends Square {
    override def connectTo(other: Square): Unit = {}
  }

  class Cat extends Node with Square {
    Edge(cat, this)
  }

  class Patient extends Node with Square {
    Edge(this, patient)
  }

  class Door extends Square {
    val entry = new Node
    val exit = new Node
    new Edge(entry, exit) {
      capacity = 1
    }

    override def connectTo(other: Square): Unit = {
      other match {
        case d: Door => Edge(exit, d.entry)
        case n: Node => Edge(exit, n)
        case _ =>
      }
    }
  }

  trait Square {
    def connectTo(other: Square): Unit = {
      val node = this.asInstanceOf[Node]
      other match {
        case d: Door => Edge(node, d.entry)
        case n: Node => Edge(node, n)
        case _ =>
      }
    }
  }

  class Node {
    val to: mutable.Set[Edge] = mutable.Set()
    val from: mutable.Set[Edge] = mutable.Set()
  }

  case class Edge(from: Node, to: Node) {
    from.to += this
    to.from += this
    var capacity = Int.MaxValue
    var used = 0
  }

  def out(line: Any) = println(line)
}
