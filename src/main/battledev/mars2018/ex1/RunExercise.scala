package battledev.mars2018.ex1

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
      input.drop(1).map { s =>
        s.split(" ").map(i => i.toDouble).sum / 3
      }.max | math.ceil | out
  }

  implicit class Extension[A](a: A) {
    def |[B](f: A => B): B = f(a)
  }

  def out(line: Any) = println(line)
}
