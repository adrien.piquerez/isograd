package battledev.mars2018.ex4

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val initialState = Set(input.map(_.toInt))
    val iter = unfold(initialState) { states =>
      Some(for {
        s <- states
        i <- 1 until 7
      } yield s.take(i).reverse ++ s.drop(i))
    }
    out {
      iter.take(7).takeWhile(set => !set.exists(s => s.dropRight(1).zip(s.drop(1)).forall{case (a, b) => a <= b})).size
    }
  }

  def unfold[A](start: A)(f: A => Option[A]): Stream[A] = start #:: (f(start) match {
    case Some(next) => unfold(next)(f)
    case None => Stream.empty
  })

  def out(line: Any) = println(line)
}
