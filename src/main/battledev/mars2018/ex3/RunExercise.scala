package battledev.mars2018.ex3

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val myNotes = input.head.split(" ").map(_.toInt)
    val k = input(2).toInt
    input.drop(3).map { s =>
      val notes = s.split(" ").map(_.toInt)
      notes.take(5).zip(myNotes).map { case (a, b) => math.abs(a - b) }.sum -> notes(5)
    }.toList.sortBy(_._1).take(k).map(_._2).sum.toDouble / k | math.floor | out
  }

  implicit class Extension[A](a: A) {
    def |[B](f: A => B): B = f(a)
  }

  def out(line: Any) = println(line)
}
