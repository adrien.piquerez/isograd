package battledev.mars2018.ex2

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val price = input.head.toDouble
    input.drop(2).map(_.toDouble).map {
      case n if n < 4 => n * price
      case n if n < 6 => n * price * 0.9
      case n if n < 10 => n * price * 0.8
      case n => n * price * 0.7
    }.sum | math.ceil | out
  }

  implicit class Extension[A](a: A) {
    def |[B](f: A => B): B = f(a)
  }

  def out(line: Any) = println(line)
}
