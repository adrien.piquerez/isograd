package battledev.mars2018.ex5

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  var matrix: Map[(Int, Int), Int] = _
  var cache: Map[(Int, Int), Int] = Map()

  def solve(input: Seq[String]) = {
    val last = input.head.toInt - 1
    matrix = input.drop(1).map(s => s.split(" ").map(_.toInt)).zipWithIndex.flatMap {case (a, x) => a.zipWithIndex.map{case (b, y) => (x, y) -> b}}.toMap
    out(max(0, last))
  }

  def max(a: Int, b: Int): Int = {
    if(b - a < 1) 0
    else if(cache.contains((a, b)))
      cache((a, b))
    else {
      val result = (a to b).map(i => matrix((a, i)) + max(a + 1, i - 1) + max(i + 1, b)).max
      cache = cache + ((a, b) -> result)
      result
    }
  }

  def out(line: Any) = println(line)
}
