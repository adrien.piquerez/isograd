package orange.sept2018.ex3

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val n = input.head.toInt
    val first = input.drop(2).head.toInt - 1
    val rounds = input.drop(3)
      .map(s => s.split(" ")
          .groupBy(k => k)
          .map( t => math.ceil(t._2.length.toDouble / 4)).sum)
      .zipWithIndex.toList
    out {
      rounds.groupBy(_._1).toList.sortBy(_._1).flatMap { case (_, list) =>
          list.map(_._2).sortBy{i => (i - first + n) % n}
      }.map( _ + 1).mkString(" ")
    }
  }

  def out(line: Any) = println(line)
}
