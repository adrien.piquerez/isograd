package orange.sept2018.ex2

class RunExercise {
  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    out { input.drop(1).flatMap(s => s.split(" ")).groupBy(s => s).map{
      case(a, seq) => (a, seq.size)
    }.maxBy(_._2)._2 }
  }

  def out(line: Any) = println(line)
}
