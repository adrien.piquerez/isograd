package orange.sept2018.ex5

import scala.collection.mutable

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val n = input.head.toInt
    val m = input.drop(1).map(s => s.split(" ").map(_.toInt)).toArray

    val nodes = (0 until n).map(_ => new Node).toArray
    var edges = (for { i<- 0 until n - 1; j <- i+ 1 until n} yield Edge(nodes(i), nodes(j), m(i)(j))).toList.sortBy(_.value)

    def all(n: Node, s: mutable.Set[Node]): Set[Node] = {
      n.edges.toStream.map(e => if (e.from == n) e.to else e.from).filter(!s.contains(_))
        .flatMap(o => all(o, s += o)).toSet + n
    }

    out {
      Stream.continually {
        val size = all(nodes.head, mutable.Set(nodes.head)).size
        if (size == nodes.length) {
          val minEdge = edges.head
          edges = edges.tail
          minEdge.remove
          Some(minEdge.value)
        }
        else None
      }.takeWhile(_.isDefined).flatten.last
    }
  }

  class Node {
    var edges = mutable.Set[Edge]()
  }

  case class Edge(from: Node, to: Node, value: Int) {
    from.edges += this
    to.edges += this

    def remove: Unit = {
      from.edges -= this
      to.edges -= this
    }
  }

  def out(line: Any) = println(line)
}
