package orange.sept2018.ex4

import scala.collection.mutable

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val n = input.head.toInt
    val matrix = input.drop(1).map(s => s.split(" ").map(_.toInt))

    val cache = mutable.Map[(Int, Set[Int]), Int]()

    def max(from: Int, rest: Set[Int]): Int = {
      if(rest.isEmpty) matrix(from)(0)
      else if(cache.contains((from, rest))) cache((from, rest))
      else {
        val result = rest.map(to => matrix(from)(to) + max(to, rest - to)).max
        cache += ((from, rest) -> result)
        result
      }
    }

    out { max(0, (1 until n).toSet) + (1 until n).map(i => matrix(i)(i)).sum }
  }

  def out(line: Any) = println(line)
}
