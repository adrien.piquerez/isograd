package orange.sept2018.ex1

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val filtered = input.drop(1).map(s => s.split(" ")).map(a => (a(0), a(1).toInt, a(2).toInt, a(3).toInt, a(4).toInt))
      .filter{case (_, age, poids, _, _) => age >= 2 && age <= 5 && poids >= 1250 && poids <= 1500}
    val result = filtered.map{case (_, _, _, n1, n2) => n1 + n2 }.max
    out { filtered.filter{case (_, _, _, n1, n2) => n1 + n2 == result}.map(v => v._1).mkString(" ") }
  }

  def out(line: Any) = println(line)
}
