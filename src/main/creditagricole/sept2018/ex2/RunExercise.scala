package creditagricole.sept2018.ex2

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val sorted = input.drop(1).map(s => s.split(" ")).map(a => (a(0).toInt, a(1).toFloat)).toList.sortBy(_._1).map(_._2)
    out {
      if (sorted.pair.forall(t => t._1 <= t._2)) "OK" else "KO"
    }
  }

  implicit class SeqExtension[A](seq: Seq[A]) {
    def pair: Seq[(A, A)] = seq.dropRight(1).zip(seq.drop(1))
  }

  def out(line: Any) = println(line)
}
