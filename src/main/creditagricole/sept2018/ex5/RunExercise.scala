package creditagricole.sept2018.ex5

import scala.collection.mutable

class RunExercise {
  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val map = mutable.Map[String, Dep]()

    input.drop(2).grouped(2).foreach {
      case Seq(sn, nd) =>
        val d = Dep(sn.split(" ").head)(mutable.Buffer())
        nd.split(" ").map(n => map.getOrElse(n, {
          val dep = Dep(n)(mutable.Buffer())
          map += (n -> dep)
          dep
        })).foreach( _.deps += d)
        map += (d.name -> d)
    }

    def deps(dep: Dep, dejaVue: mutable.Set[Dep]): Set[Dep] = {
      dep.deps.toSet.diff(dejaVue).flatMap{d => deps(d, dejaVue += d)} ++ dep.deps
    }

    val modified = map.getOrElse(input.head, Dep(input.head)(mutable.Buffer()))
    val res = deps(modified, mutable.Set(modified))

    out { res.size }
    out { res.map(d => d.name)}
  }

  case class Dep(name: String)(val deps: mutable.Buffer[Dep])

  def out(line: Any) = println(line)
  def out(lines: Traversable[Any]) = lines.foreach(println)
}
