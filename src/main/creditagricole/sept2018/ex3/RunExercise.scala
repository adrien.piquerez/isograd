package creditagricole.sept2018.ex3

class RunExercise {

  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val n = input.head.toInt
    val dir = input(1)
    val grid = (for (i <- 0 until n) yield (
      (0 until 5 - i).map(_ => '.') ++ (0 until 2*i + 1).map(_ => '*') ++ (0 until 5 - i).map(_ => '.')).mkString("")
    ) ++ (for (i <- n until 11) yield (0 until 11).map(_ => '.').mkString(""))

    out {
      dir match {
        case "N" => grid
        case "S" => grid.reverse
        case "O" => grid.transpose.map(_.mkString(""))
        case "E" => grid.reverse.transpose.map(_.mkString(""))
      }
    }
  }

  def out(lines: Traversable[Any]) = lines.foreach(println)
}
