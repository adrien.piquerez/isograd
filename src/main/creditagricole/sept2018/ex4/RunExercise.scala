package creditagricole.sept2018.ex4

import scala.collection.SortedSet

class RunExercise {
  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    val grid = SortedSet((for (i <- 0 until 10; j <- 0 until 10) yield (i, j)): _*)
    val pieces = input.drop(1).map(s => s.split(" ")).map(a => Piece(a(0), a(1).toInt, a(2).toInt)).toSet

    out { fill(grid, pieces).head.map { case (p, (x, y)) => s"${p.name} $x $y"} }
  }

  def fill(grid: SortedSet[(Int, Int)], pieces: Set[Piece]): Stream[Seq[(Piece, (Int, Int))]] = {
    if (grid.isEmpty) Stream(Seq())
    else {
      val (x, y) = grid.head
      pieces.toStream.flatMap { p =>
        val places = for {
          i <- x until x + p.w
          j <- y until y + p.h
        } yield (i, j)
        if (places.forall(pl => grid.contains(pl)))
          fill(grid -- places, pieces - p).map(s => s :+ (p -> (x, y)))
        else Stream()
      }
    }
  }

  case class Piece(name: String, w: Int, h: Int)

  def out(lines: Traversable[Any]) = lines.foreach(println)
}
