package creditagricole.sept2018.ex1

class RunExercise {
  def contestResponse() = {
    val input = io.Source.stdin.getLines.toSeq
    solve(input)
  }

  def solve(input: Seq[String]) = {
    out {
      14 * 60 - input.drop(1).filter(s => s.startsWith("S")).map(s => s.split(" ")(1).toInt).sum
    }
  }

  def out(line: Any) = println(line)
}
